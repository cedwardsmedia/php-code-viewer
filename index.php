<?
/* Portions Copyright (c) 1997-2014 Corey Edwards All Rights
 * Reserved.
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Corey Edwards Public Source
 * License Version 3.0 (the 'License'). You may not use this file except
 * in compliance with the License. Please obtain a copy of the License at
 * http://www.cedwardsmedia.com/cepsl/ and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESSED OR IMPLIED, AND COREY EDWARDS HEREBY DISCLAIMS ALL SUCH
 * WARRANTIES, INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR
 * NON-INFRINGEMENT. Please see the License for the specific language
 * governing rights and limitations under the License.
*/

// Get our file from the URL and set it to $file
$file = $_GET["file"];

// For sanity, ensure our file is in our current directory.
// Otherwise, one could view any file on the system.
if (dirname($file) != "."){
        die("Access denied");
}

// Check that the file exists. If not, die().
if (!file_exists($file)){
    die("<html><head><title>No code found</title><style>body{background:#00a;color:#fff;font-family:courier;font-size:12pt;text-align:center;margin:100px;}.neg{background:#fff;color:#00a;padding:2px 8px;font-weight:700}</style></head><body><span class=\"neg\">ERROR 404</span><p>The file you are looking for cannot be found.</p></body></html>");
};

// Load the contents of the file into $code
$code = file_get_contents($file);
?>
<html>
    <head>
        <title>Corey's Codes - Viewing <? echo $file ?></title>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
        <style>html{font-size:20px;padding:15px;background-color:#000;}.str{color:#EC7600}.kwd{color:#93C763}.com{color:#66747B}.typ{color:#678CB1}.lit{color:#FACD22}.pln,.pun{color:#F1F2F3}.tag{color:#8AC763}.atn{color:#E0E2E4}.atv{color:#EC7600}.dec{color:purple}pre.prettyprint{border:0 solid #888}ol.linenums{margin-top:0;margin-bottom:0}.prettyprint{background:#000}li.L0,li.L1,li.L2,li.L3,li.L4,li.L5,li.L6,li.L7,li.L8,li.L9{color:#555;list-style-type:decimal}li.L1,li.L3,li.L5,li.L7,li.L9{background:#111}@media print{.str{color:#060}.kwd{color:#006;font-weight:700}.com{color:#600;font-style:italic}.typ{color:#404;font-weight:700}.lit{color:#044}.pun{color:#440}.pln{color:#000}.tag{color:#006;font-weight:700}.atn{color:#404}.atv{color:#060}}</style>
    </head>
    <body>
        <pre class="prettyprint lang-bash linenums">
<code><? echo $code; ?></code>
        </pre>
        <div style="text-align:center;">
            <code>
                <span class="com">
                    # &copy; 2014 Corey Edwards - All Rights Reserved.
                </span>
            </code>
        </div>
    </body>
</html>